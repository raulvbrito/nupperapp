angular.module('nupper', ['ionic', 'chart.js', 'nupper.controllers', 'timer', 'ngIOS9UIWebViewPatch'])

 .run(function($ionicPlatform) {
   $ionicPlatform.ready(function() {
     // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
     // for form inputs)
     if (window.cordova && window.cordova.plugins.Keyboard) {
       cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
       cordova.plugins.Keyboard.disableScroll(true);

     }
     if (window.StatusBar) {
       // org.apache.cordova.statusbar required
       StatusBar.style(1);
     }
   });
 })

 .config(function($stateProvider, $urlRouterProvider) {
   $stateProvider

     .state('app', {
     url: '/app',
     abstract: true,
     templateUrl: 'templates/menu.html',
     controller: 'AppCtrl'
   })

   .state('login', {
     url: '/login',
     templateUrl: 'templates/login.html',
     controller: 'LoginCtrl'
   })
    .state('app.overview', {
       url: '/overview',
       views: {
         'menuContent': {
            templateUrl: 'templates/overview.html',
            controller: 'OverviewCtrl'
         }
       }
     })
     .state('app.timeline', {
       url: '/timeline',
       views: {
         'menuContent': {
            templateUrl: 'templates/timeline.html',
            controller: 'TimelineCtrl'
         }
       }
     })
     .state('app.profile', {
       url: '/profile',
       views: {
         'menuContent': {
           templateUrl: 'templates/profile.html',
           controller: 'ProfileCtrl'
         }
       }
     })
     .state('app.recipes', {
       url: '/recipes',
       views: {
         'menuContent': {
            templateUrl: 'templates/recipes.html',
            controller: 'RecipesCtrl'
         }
       }
     })
     .state('app.recipe', {
        url: '/recipes/:recipeId',
        views: {
           'menuContent': {
             templateUrl: 'templates/recipe.html',
             controller: 'RecipeCtrl'
           }
        }
     });

     var user_info = localStorage.getItem('user_info');
     var initial_route;

     if(user_info)
         initial_route = "/app/overview";
     else
         initial_route = "/login";

     $urlRouterProvider.otherwise(initial_route.toString());
 })

 .factory('Authorization', function($rootScope) {
          authorization = {};
          authorization.title = '';
          authorization.description = '';
          return authorization;
 })

 .directive('iconSwitcher', function() {
         return {
            restrict : 'A',

            link : function(scope, elem, attrs) {

            var currentState = true;

            elem.on('click', function() {

              if(currentState === true) {
                    angular.element(elem).removeClass(attrs.onIcon);
                    angular.element(elem).addClass(attrs.offIcon);
              } else {
                    angular.element(elem).removeClass(attrs.offIcon);
                    angular.element(elem).addClass(attrs.onIcon);
              }

              currentState = !currentState
            });
         }
     };
 });

