 angular.module('nupper.controllers', ['ngCordova'])

 .controller('LoginCtrl', function($scope, $state, $ionicModal, $ionicPopup, $cordovaOauth, $http) {
             $('#login-content .slider').css('height', $(document).height());

             $ionicModal.fromTemplateUrl('templates/cadastro.html', {
                 scope: $scope
             }).then(function(modal) {
                 $scope.modal = modal;
             });

             $scope.closeCadastro = function() {
                 $scope.modal.hide();
             };

             $scope.cadastro = function() {
                 $scope.modal.show();
                 if($(document).height() < 600){
                     $('.modal-form#cadastro-form .login-form').css('top','140px');
                     $('.policy').css('margin-top', '1.8rem');
                 }
             };

             $ionicModal.fromTemplateUrl('templates/email-login.html', {
                 scope: $scope
             }).then(function(modal) {
                 $scope.loginModal = modal;
             });

             $scope.closeLogin = function() {
                 $scope.loginModal.hide();
             };

             $scope.showLoginModal = function() {
                 $scope.loginModal.show();
                 if($(document).height() >= 600)
                     $('.modal-form#login-form .login-form').css('top','270px');
             };

             window.cordovaOauth = $cordovaOauth;
             window.http = $http;

             $scope.fbLogin = function(){
                 $cordovaOauth.facebook("1028724903817373", ["email", "public_profile"], {redirect_uri: "http://localhost/callback"}).then(function(result){
                     $scope.getFacebookData(result.access_token);
                 },  function(error){
                     console.log(error);
                 });
             }

             $scope.getFacebookData = function(access_token){
                 $http.get("https://graph.facebook.com/v2.2/me", {params: {access_token: access_token, fields: "name,gender,location,picture.type(large)", format: "json" }}).then(function(result) {
                     var name = result.data.name;
                     var gender = result.data.gender;
                     var picture = result.data.picture;

                     var user_info = '{ "user_info" : { "user": "@'+name.replace(' ','').toLowerCase()+'", "username":"'+name+'", "gender":"'+gender+'", "picture":"'+picture.data.url+'" } }';

                     localStorage.setItem('user_info', user_info);

                     $state.go('app.profile');
                 }, function(error) {
                     console.log(error);
                 });
             }

             $scope.googleLogin = function(){
             $cordovaOauth.google("59733441181-3mhvvn4sr76c1j9fc10j1qk5u3p8etnh.apps.googleusercontent.com", ["https://www.googleapis.com/auth/plus.login", "https://www.googleapis.com/auth/userinfo.email"]).then(function(result) {
                                                                                                                                                             $scope.getGoogleData(result.access_token);
                                                                                                                                                             }, function(error) {
                                                                                                                                                             console.log(error);
                                                                                                                                                             });
             }

             $scope.getGoogleData = function(access_token){
             $http.get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json", {params: {access_token: access_token, fields: "name,gender,email,picture", format: "json" }}).then(function(result) {
                 var name = result.data.name;
                 var gender = result.data.gender;
                 var picture = result.data.picture;

                                                                                                                                                                                    console.log(picture);
                 var user_info = '{ "user_info" : { "user": "@'+name.replace(' ','').toLowerCase()+'", "username":"'+name+'", "gender":"'+gender+'", "picture":"'+picture+'" } }';

                 console.log(user_info);
                 console.log(JSON.parse(user_info));

                 localStorage.setItem('user_info', user_info);

                 $state.go('app.profile');
             }, function(error) {
                 console.log(error);
             });
             }

             $scope.login = function(){
                 $state.go('app.timeline');
 //                LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
 //                                                                                       $scope.submitting = true;                                        $state.go('app.perfil');
 //                                                                                       }).error(function(data) {
 //                                                                                                $scope.submitting = true;                                          console.log(data);
 //                                                                                                $state.go('app.profile');
 //                                                                                                /*var alertPopup = $ionicPopup.alert({
 //                                                                                                 title: 'Falha no login',
 //                                                                                                 template: 'Usuário ou senha incorretos'
 //                                                                                                 });*/
 //                                                                                                });
 //            });
             };

             $scope.forgotPassword = function() {
             $scope.data = {}

             // An elaborate, custom popup
             var myPopup = $ionicPopup.show({
                                            template: '<input type="text">',
                                            title: 'Recupere sua senha',
                                            subTitle: 'Insira seu endereço de email abaixo para te mandarmos um email para recuperar sua senha',
                                            scope: $scope,
                                            buttons: [
                                                      {
                                                      text: 'Cancelar',
                                                      type: 'button-clear'
                                                      },
                                                      {
                                                      text: '<b>Recuperar</b>',
                                                      type: 'button-clear',
                                                      onTap: function(e) {
                                                      }
                                                      }
                                                      ]
                                            });
             myPopup.then(function(res) {
                          });
             $timeout(function() {
                      myPopup.close(); //close the popup after 3 seconds for some reason
                      }, 3000);
             };

             $scope.touchIDLogin = function(){
                 $cordovaTouchID.checkSupport().then(function(result) {
                                                 console.log(result);
                                                     $cordovaTouchID.authenticate("text").then(function(result) {
                                                                                               console.log(result);
                                                                                               }, function (error) {
                                                                                               console.log(error);
                                                                                               });

                                                 }, function (error) {
                                                 console.log(error);
                                                 });
             }
 })

 .controller('AppCtrl', function($scope, $state, $ionicModal, $timeout, $rootScope, $ionicSideMenuDelegate) {
             $scope.rightButton = 'ion-ios-bookmarks';

             $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
                            switch(toState.name){
                            case 'app.timeline': $scope.rightButton = 'ion-ios-bookmarks'; break;
                            case 'app.profile': $scope.rightButton = 'ion-edit'; break;
                            default: $scope.rightButton = ''; break;
                            };
             })


   $scope.loginData = {};

   $ionicModal.fromTemplateUrl('templates/edit-profile.html', {
     scope: $scope
   }).then(function(modal) {
     $scope.modal = modal;
   });

   $scope.closeLogin = function() {
     $scope.modal.hide();
   };

   $scope.editProfile = function() {
     $scope.modal.show();
   };

   $scope.doLogin = function() {
     console.log('Doing login', $scope.loginData);

     $timeout(function() {
       $scope.closeLogin();
     }, 1000);
   };

   $scope.logout = function(){
       $ionicSideMenuDelegate.toggleLeft();

       localStorage.removeItem('user_info');

       $state.go('login');
   }
 })

 .controller('ProfileCtrl', function($scope, $state, $timeout, $ionicModal, $ionicSlideBoxDelegate, $ionicActionSheet, $cordovaImagePicker, $cordovaCamera, $ionicPopup) {
             var user_info = localStorage.getItem('user_info');

             var username = JSON.parse(user_info).user_info.username;
             var user = JSON.parse(user_info).user_info.user;
             var picture = JSON.parse(user_info).user_info.picture;

             if(user_info){
                 document.getElementById('profile-img').style.backgroundImage = 'url('+picture+')';
                 $scope.profileName = username;
                 $scope.profileUserName = user;
             }else{
                 $state.go('login');
             }

             $scope.meals = [
                               { image: 'breakfast', title: 'Café da Manhã', time: '9h - 200 cal (10%)', id: 1 },
                               { image: 'lunch', title: 'Almoço', time: '12h - 900 cal (45%)', id: 2 },
                               { image: 'dinner', title: 'Jantar', time: '20h - 700 cal (35%)', id: 3 }
             ];

             $(".meter > span").each(function() {
                                     $(this)
                                     .data("origWidth", $(this).width())
                                     .width(0)
                                     .animate({
                                              width: $(this).data("origWidth")
                                              }, 1200);
                                     });
             $timeout(function(){
                 $('.meter > span').css('width', '33.3%');
             }, 300);
             //Doughut Chart
 //            $scope.labels = ["Proteínas", "Carboidratos", "Gorduras"];
 //            $scope.data = [300, 500, 100];
 //            $scope.colours = ["#FF9900", "#FFE500", "#CCCCCC"];
 //            $scope.options = {
 //            animationEasing: "easeOutQuart",
 //            showTooltips: false,
 //            percentageInnerCutout: 93,
 //            segmentShowStroke : false,
 //            onAnimationProgress: function () {
 //
 //            this.chart.ctx.font = '300 2.5rem Helvetica Neue';
 //            this.chart.ctx.fillStyle = '#666';
 //            this.chart.ctx.textAlign = 'center';
 //            this.chart.ctx.textBaseline = 'middle';
 //
 //            var valueLabel = this.segments[0].value + this.segments[1].value;
 //
 //            var x = this.chart.canvas.clientWidth / 2;
 //            var y = this.chart.canvas.clientHeight / 2;
 //
 //            var x_fix = 0;
 //            var y_fix = 0;
 //
 //            this.chart.ctx.fillText(valueLabel, x + x_fix, y + y_fix - 10);
 //
 //            if($(document).height() < 600){
 //                $('#chart-total-label').css({
 //                 'margin-top': '-5rem',
 //                 'padding-bottom': '5.5rem'
 //                });
 //            }
 //                $('#chart-total-label').css('visibility', 'visible');
 //            }
 //
 //            };


             if($(document).height() < 600)
                 $('.goals-container .chart-container canvas').css('height','200px');

             $scope.renderProgress = function(progress){
             progress = Math.floor(progress);
             if(progress<25){
             var angle = -90 + (progress/100)*360;
             $(".animate-0-25-b").css({"transform":"rotate("+angle+"deg)", "transition":"transform 1.5s ease"});
             }
             else if(progress>=25 && progress<50){
             var angle = -90 + ((progress-25)/100)*360;
             $(".animate-0-25-b").css({"transform":"rotate(0deg)", "transition":"transform 1.5s ease"});
             $(".animate-25-50-b").css({"transform":"rotate("+angle+"deg)", "transition":"transform 1.5s ease"});
             }
             else if(progress>=50 && progress<75){
             var angle = -90 + ((progress-50)/100)*360;
             $(".animate-25-50-b, .animate-0-25-b").css({"transform":"rotate(0deg)", "transition":"transform 1.5s ease"});
             $(".animate-50-75-b").css({"transform":"rotate("+angle+"deg)", "transition":"transform 1.5s ease"});
             }
             else if(progress>=75 && progress<=100){
             var angle = -90 + ((progress-75)/100)*360;
             $(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b")
             .css({"transform":"rotate(0deg)", "transition":"transform 1.5s ease"});
             $(".animate-75-100-b").css({"transform":"rotate("+angle+"deg)", "transition":"transform 1.5s ease"});
             }
             }

             $scope.slideHasChanged = function(index){
                 if(index == 2){
                     $timeout(function(){
                      $scope.renderProgress(87);
                      }, 100);
                 }else if(index == 1){
                     $('.circle-container').css('margin-left', '20%');
                     $('.weight-lost').css('width', '30%');
                 }
             }
             //Radar chart

 //            $scope.labels =["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"];
 //
 //            $scope.data = [
 //                           [65, 59, 90, 81, 56, 55, 40],
 //                           [28, 48, 40, 19, 96, 27, 100]
 //                           ];

             $scope.previousSlide = function(){
                 $ionicSlideBoxDelegate.previous();
             }

             $scope.nextSlide = function(){
                 $ionicSlideBoxDelegate.next();
             }

             $scope.changePicture = function(){
                 $ionicActionSheet.show({
                                    buttons: [
                                              { text: 'Carregar Foto do Dispositivo' },
                                              { text: 'Tirar Foto' },
                                              { text: 'Ver Foto' }
                                              ],
                                    destructiveText: 'Remover Foto',
                                    titleText: 'Foto de Perfil',
                                    cancelText: 'Cancelar',
                                    cancel: function() {

                                    },
                                    buttonClicked: function(index) {
                                        switch(index){
                                         case 0:
                                             $scope.pickImage();
                                             break;
                                         case 1:
                                             $scope.takePicture();
                                             break;
                                        }
                                        return true;
                                    }
                                    });
             }

             $scope.pickImage = function(){
                 var options = {
                     maximumImagesCount: 10,
                     width: 800,
                     height: 800,
                     quality: 80
                 };

                 $cordovaImagePicker.getPictures(options)
                 .then(function (results) {
                   for (var i = 0; i < results.length; i++) {
                     document.getElementById('profile-img').style.backgroundImage = 'url('+results[i]+')';
                   }
                   }, function(error) {
                       alert('Não foi possível pegar uma imagem');
                   });
             }

             $scope.takePicture = function(){
                 var options = {
                     destinationType: Camera.DestinationType.FILE_URI,
                     sourceType: Camera.PictureSourceType.CAMERA,
                 };

                 $cordovaCamera.getPicture(options).then(function(imageURI) {
                     document.getElementById('profile-img').style.backgroundImage = 'url('+imageURI+')';
                 }, function(err) {

                 });
             }

             $scope.recipes = function(){
                 $state.go('app.recipes');
             }

             $ionicModal.fromTemplateUrl('templates/weight-modal.html', {
                     scope: $scope
             }).then(function(modal) {
                     $scope.modal = modal;
             });

             $scope.weightModal = function(){

                 $scope.weights = [
                                   { icon: 'ion-arrow-down-a green', number: '77.9kg', date: '30/10', id: 5 },
                                   { icon: 'ion-minus-round light-orange', number: '79.5kg', date: '21/10', id: 4 },
                                   { icon: 'ion-arrow-up-a red', number: '79.5kg', date: '14/10', id: 3 },
                                   { icon: 'ion-arrow-down-a green', number: '79kg', date: '07/10', id: 2 },
                                   { icon: 'ion-minus-round gray', number: '80kg', date: '01/10 - início', id: 1 }
                 ];

                 $scope.modal.show();

             $scope.lineLabels = ["01/10", "07/10", "14/10", "21/10", "30/10"];
             $scope.lineData = [[80, 79, 79.5, 79.5, 77.9]];
             $scope.lineColours = ["#ffbf05", "#FFE500", "#CCCCCC"];
             $scope.lineSeries = ['Peso'];
             $scope.lineOptions = {
             bezierCurve : false,
             scaleShowGridLines : false,
             scaleLineColor : "rgba(225, 225, 225, 0.29)",
             scaleShowHorizontalLines: false,
             responsive: true,
             scaleShowVerticalLines: false,
             scaleStartValue: 0
             };
             };

             $scope.updateWeight = function(){
             var myPopup = $ionicPopup.show({
                                            template: '<input type="text">',
                                            title: 'Atualize seu peso',
                                            subTitle: 'Insira abaixo o seu peso atual',
                                            scope: $scope,
                                            buttons: [
                                                      {
                                                      text: 'Cancelar',
                                                      type: 'button-clear'
                                                      },
                                                      {
                                                      text: '<b>Registrar</b>',
                                                      type: 'button-clear',
                                                      onTap: function(e) {
                                                      }
                                                      }
                                                      ]
                                            });
             myPopup.then(function(res) {
                          console.log(res);
                          });
             };

             $scope.closeModal = function() {
                 $scope.modal.hide();
             };
 })

 .controller('TimelineCtrl', function($scope, $http, $ionicActionSheet, $cordovaImagePicker, $cordovaCamera, $timeout, $compile) {
             var user_info = localStorage.getItem('user_info');

             var username = JSON.parse(user_info).user_info.username;
             var user = JSON.parse(user_info).user_info.user;
             var picture = JSON.parse(user_info).user_info.picture;

             if(user_info){
                 $('.profile-block #timeline-profile-img').css('background-image', 'url('+picture+')');
                 $('.profile-block #timeline-profile-name').html(user);
             }else{
                 $state.go('login');
             }


             $scope.doRefresh = function() {
 //            $http.get('/new-items')
 //            .success(function(newItems) {
 //                     $scope.items = newItems;
 //                     })
 //            .finally(function() {
 //                     $scope.$broadcast('scroll.refreshComplete');
 //                     });
             $timeout(function() {
                      $scope.$broadcast('scroll.refreshComplete');
             }, 3000);
             }

             $scope.photoToShare = function(){
                 $ionicActionSheet.show({
                                    buttons: [
                                              { text: 'Carregar Foto do Dispositivo' },
                                              { text: 'Tirar Foto' },
                                              { text: 'Antes e Depois' }
                                              ],
                                    titleText: 'Compartilhar Foto',
                                    cancelText: 'Cancelar',
                                    cancel: function() {

                                    },
                                    buttonClicked: function(index) {
                                    switch(index){
                                    case 0:
                                    $scope.pickImage();
                                    break;
                                    case 1:
                                    $scope.takePicture();
                                    break;
                                    case 2:
                                    $scope.beforeAfter(0);
                                    }
                                    return true;
                                    }
                 });
             }

             $scope.pickImage = function(){
             var options = {
             maximumImagesCount: 10,
             width: 800,
             height: 800,
             quality: 80
             };

             $cordovaImagePicker.getPictures(options)
             .then(function (results) {
                   for (var i = 0; i < results.length; i++) {
                   document.getElementById('photo-to-share').style.backgroundImage = 'url('+results[i]+')';
                   document.getElementById('photo-to-share').style.display = 'block';
                   }
                   }, function(error) {
                   alert('Não foi possível pegar uma imagem');
                   });
             }

             $scope.takePicture = function(){
             var options = {
             destinationType: Camera.DestinationType.FILE_URI,
             sourceType: Camera.PictureSourceType.CAMERA,
             correctOrientation: true,
             encodingType: Camera.EncodingType.JPEG,
             targetWidth: 720
             };

             $cordovaCamera.getPicture(options).then(function(imageURI) {
                                                     document.getElementById('photo-to-share').style.backgroundImage = 'url('+imageURI+')';
                                                     document.getElementById('photo-to-share').style.display = 'block';
                                                     }, function(err) {

                                                     });
             }

             $scope.beforeAfter = function(index){
             var options = {
             destinationType: Camera.DestinationType.FILE_URI,
             sourceType: Camera.PictureSourceType.CAMERA,
             correctOrientation: true,
             encodingType: Camera.EncodingType.JPEG,
             targetWidth: 720
             };

             $cordovaCamera.getPicture(options).then(function(imageURI) {
                                                     document.getElementById('photo-to-share-'+index).style.backgroundImage = 'url('+imageURI+')';
                                                     document.getElementById('photo-to-share-'+index).style.display = 'block';

                                                     if(index == 0)
                                                     $scope.beforeAfter(1);

                                                     }, function(err) {

                                                     });
             }

             $scope.post = function(){
             var postStatus = $('.profile-block').clone();

             var compiledElement = $compile(postStatus)($scope);

             postStatus.find('.photo-to-share').css({
                                                    'background-image': 'none',
                                                    'display': 'none'
                                                    });

             var postContent = $('.cd-timeline-block.profile-block textarea').val();
             $('.cd-timeline-block.profile-block textarea').parent().parent().parent().append('<p>'+postContent+'</p>');

             $('.cd-timeline-block.profile-block textarea').parent().parent().remove();

             $('.cd-timeline-block.profile-block .cd-date').fadeIn();

             $('.cd-timeline-block.profile-block .ion-ios-heart-outline').fadeIn();

             $('.cd-timeline-block.profile-block').removeClass('profile-block recent-post');

             $('.profile-img').removeClass('profile-img-share');

             $('.share-options').css({
                                     'transform': 'scaleY(0)',
                                     'transform-origin': 'top',
                                     'transition': 'transform 0.5s ease'
                                     }).remove();

             setTimeout(function(){
                        postStatus.addClass('recent-post').css({
                                                               'max-height': '0',
                                                               'transform': 'scaleY(0)',
                                                               'transform-origin': 'top',
                                                               'transition': 'all 0.1s ease'
                                                               });

                        $('.cd-container').prepend(postStatus);

                        setTimeout(function(){


                                   $('.recent-post').css({
                                                         'max-height': '500px',
                                                         'transform': 'scaleY(1)',
                                                         'transform-origin': 'top',
                                                         'transition': 'all 0.3s cubic-bezier(.44,.44,.74,.73)'
                                                         });
                                   }, 1);
                        }, 100);
             }

 })

 .controller('RecipesCtrl', function($scope, $state, Authorization){
     $scope.recipes = [
         { image: 'img/bolo_cenoura.png', title: 'Bolo de Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 1 },
         { image: 'img/bife_cenoura.png', title: 'Bife Enrolado com Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 2 },
         { image: 'img/hamburguer_cenoura.png', title: 'Hamburguer de Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 3 },
         { image: 'img/salada_cenoura.png', title: 'Salada de grão-de-bico e cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 4 },
         { image: 'img/salada_frango_cenoura.png', title: 'Salada de Frango com Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 5 },
         { image: 'img/suco_cenoura.png', title: 'Suco de Cenoura com Beterraba', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 6 },
         { image: 'img/suco_laranja_cenoura.png', title: 'Suco de Laranja com Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 7 },
         { image: 'img/sufle_cenoura.png', title: 'Suflê de Cenoura', description: 'Deliciosa e simples de preparar', id: 8 },
         { image: 'img/bolo_cenoura.png', title: 'Bolo de Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 9 },
         { image: 'img/bolo_cenoura.png', title: 'Bolo de Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 10 }
     ];

             $scope.teste = Authorization;
             console.log($scope.teste);
     $scope.doRefresh = function() {
         $timeout(function() {
             $scope.$broadcast('scroll.refreshComplete');
         }, 3000);
     }
 })

 .controller('RecipeCtrl', function($scope, $stateParams, $ionicHistory, Authorization){
             $scope.teste = Authorization;

             console.log($scope.teste);
             console.log($stateParams);
             console.log($stateParams.recipeId);

             $scope.renderProgress = function(progress){
             progress = Math.floor(progress);
             if(progress<25){
             var angle = -90 + (progress/100)*360;
             $(".animate-0-25-b").css("transform","rotate("+angle+"deg)");
             }
             else if(progress>=25 && progress<50){
             var angle = -90 + ((progress-25)/100)*360;
             $(".animate-0-25-b").css("transform","rotate(0deg)");
             $(".animate-25-50-b").css("transform","rotate("+angle+"deg)");
             }
             else if(progress>=50 && progress<75){
             var angle = -90 + ((progress-50)/100)*360;
             $(".animate-25-50-b, .animate-0-25-b").css("transform","rotate(0deg)");
             $(".animate-50-75-b").css("transform","rotate("+angle+"deg)");
             }
             else if(progress>=75 && progress<=100){
             var angle = -90 + ((progress-75)/100)*360;
             $(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b")
             .css("transform","rotate(0deg)");
             $(".animate-75-100-b").css("transform","rotate("+angle+"deg)");
             }
             $(".text").html("1500/2000<br>calorias");
             }

             $scope.renderProgress(75);
 });


 angular.module('nupper.controllers', ['ngCordova'])


 .controller('LoginCtrl', function($scope, $state, $ionicModal, $ionicPopup, $cordovaOauth, $http) {
             $('#login-content .slider').css('height', $(document).height());

             $ionicModal.fromTemplateUrl('templates/cadastro.html', {
                 scope: $scope
             }).then(function(modal) {
                 $scope.modal = modal;
             });

             $scope.closeCadastro = function() {
                 $scope.modal.hide();
             };

             $scope.cadastro = function() {
                 $scope.modal.show();
                 if($(document).height() < 600){
                     $('.modal-form#cadastro-form .login-form').css('top','140px');
                     $('.policy').css('margin-top', '1.8rem');
                 }
             };

             $ionicModal.fromTemplateUrl('templates/email-login.html', {
                 scope: $scope
             }).then(function(modal) {
                 $scope.loginModal = modal;
             });

             $scope.closeLogin = function() {
                 $scope.loginModal.hide();
             };

             $scope.showLoginModal = function() {
                 $scope.loginModal.show();
                 if($(document).height() >= 600)
                     $('.modal-form#login-form .login-form').css('top','270px');
             };

             window.cordovaOauth = $cordovaOauth;
             window.http = $http;

             $scope.fbLogin = function(){
                 $cordovaOauth.facebook("1028724903817373", ["email", "public_profile"], {redirect_uri: "http://localhost/callback"}).then(function(result){
                     $scope.getFacebookData(result.access_token);
                 },  function(error){
                     console.log(error);
                 });
             }

             $scope.getFacebookData = function(access_token){
                 $http.get("https://graph.facebook.com/v2.2/me", {params: {access_token: access_token, fields: "name,gender,location,picture.type(large)", format: "json" }}).then(function(result) {
                     var name = result.data.name;
                     var gender = result.data.gender;
                     var picture = result.data.picture;

                     var user_info = '{ "user_info" : { "user": "@'+name.replace(' ','').toLowerCase()+'", "username":"'+name+'", "gender":"'+gender+'", "picture":"'+picture.data.url+'" } }';

                     localStorage.setItem('user_info', user_info);

                     $state.go('app.profile');
                 }, function(error) {
                     console.log(error);
                 });
             }

             $scope.googleLogin = function(){
             $cordovaOauth.google("59733441181-3mhvvn4sr76c1j9fc10j1qk5u3p8etnh.apps.googleusercontent.com", ["https://www.googleapis.com/auth/plus.login", "https://www.googleapis.com/auth/userinfo.email"]).then(function(result) {
                                                                                                                                                             $scope.getGoogleData(result.access_token);
                                                                                                                                                             }, function(error) {
                                                                                                                                                             console.log(error);
                                                                                                                                                             });
             }

             $scope.getGoogleData = function(access_token){
             $http.get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json", {params: {access_token: access_token, fields: "name,gender,email,picture", format: "json" }}).then(function(result) {
                 var name = result.data.name;
                 var gender = result.data.gender;
                 var picture = result.data.picture;

                                                                                                                                                                                    console.log(picture);
                 var user_info = '{ "user_info" : { "user": "@'+name.replace(' ','').toLowerCase()+'", "username":"'+name+'", "gender":"'+gender+'", "picture":"'+picture+'" } }';

                 console.log(user_info);
                 console.log(JSON.parse(user_info));

                 localStorage.setItem('user_info', user_info);

                 $state.go('app.profile');
             }, function(error) {
                 console.log(error);
             });
             }

             $scope.login = function(){
                 $state.go('app.timeline');
 //                LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
 //                                                                                       $scope.submitting = true;                                        $state.go('app.perfil');
 //                                                                                       }).error(function(data) {
 //                                                                                                $scope.submitting = true;                                          console.log(data);
 //                                                                                                $state.go('app.profile');
 //                                                                                                /*var alertPopup = $ionicPopup.alert({
 //                                                                                                 title: 'Falha no login',
 //                                                                                                 template: 'Usuário ou senha incorretos'
 //                                                                                                 });*/
 //                                                                                                });
 //            });
             };

             $scope.forgotPassword = function() {
             $scope.data = {}

             // An elaborate, custom popup
             var myPopup = $ionicPopup.show({
                                            template: '<input type="text">',
                                            title: 'Recupere sua senha',
                                            subTitle: 'Insira seu endereço de email abaixo para te mandarmos um email para recuperar sua senha',
                                            scope: $scope,
                                            buttons: [
                                                      {
                                                      text: 'Cancelar',
                                                      type: 'button-clear'
                                                      },
                                                      {
                                                      text: '<b>Recuperar</b>',
                                                      type: 'button-clear',
                                                      onTap: function(e) {
                                                      }
                                                      }
                                                      ]
                                            });
             myPopup.then(function(res) {
                          });
             $timeout(function() {
                      myPopup.close(); //close the popup after 3 seconds for some reason
                      }, 3000);
             };

             $scope.touchIDLogin = function(){
                 $cordovaTouchID.checkSupport().then(function(result) {
                                                 console.log(result);
                                                     $cordovaTouchID.authenticate("text").then(function(result) {
                                                                                               console.log(result);
                                                                                               }, function (error) {
                                                                                               console.log(error);
                                                                                               });

                                                 }, function (error) {
                                                 console.log(error);
                                                 });
             }
 })

 .controller('AppCtrl', function($scope, $state, $ionicModal, $timeout, $rootScope, $ionicSideMenuDelegate) {
             $scope.rightButton = 'ion-ios-bookmarks';

             $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
                            switch(toState.name){
                            case 'app.timeline': $scope.rightButton = 'ion-ios-bookmarks'; break;
                            case 'app.profile': $scope.rightButton = 'ion-edit'; break;
                            default: $scope.rightButton = ''; break;
                            };
             })


   $scope.loginData = {};

   $ionicModal.fromTemplateUrl('templates/edit-profile.html', {
     scope: $scope
   }).then(function(modal) {
     $scope.modal = modal;
   });

   $scope.closeLogin = function() {
     $scope.modal.hide();
   };

   $scope.editProfile = function() {
     $scope.modal.show();
   };

   $scope.doLogin = function() {
     console.log('Doing login', $scope.loginData);

     $timeout(function() {
       $scope.closeLogin();
     }, 1000);
   };

   $scope.logout = function(){
       $ionicSideMenuDelegate.toggleLeft();

       localStorage.removeItem('user_info');

       $state.go('login');
   }
 })

 .controller('ProfileCtrl', function($scope, $state, $timeout, $ionicModal, $ionicSlideBoxDelegate, $ionicActionSheet, $cordovaImagePicker, $cordovaCamera, $ionicPopup) {
             var user_info = localStorage.getItem('user_info');

             var username = JSON.parse(user_info).user_info.username;
             var user = JSON.parse(user_info).user_info.user;
             var picture = JSON.parse(user_info).user_info.picture;

             if(user_info){
                 document.getElementById('profile-img').style.backgroundImage = 'url('+picture+')';
                 $scope.profileName = username;
                 $scope.profileUserName = user;
             }else{
                 $state.go('login');
             }

             $scope.meals = [
                               { image: 'breakfast', title: 'Café da Manhã', time: '9h - 200 cal (10%)', id: 1 },
                               { image: 'lunch', title: 'Almoço', time: '12h - 900 cal (45%)', id: 2 },
                               { image: 'dinner', title: 'Jantar', time: '20h - 700 cal (35%)', id: 3 }
             ];

             $(".meter > span").each(function() {
                                     $(this)
                                     .data("origWidth", $(this).width())
                                     .width(0)
                                     .animate({
                                              width: $(this).data("origWidth")
                                              }, 1200);
                                     });
             $timeout(function(){
                 $('.meter > span').css('width', '33.3%');
             }, 300);
             //Doughut Chart
 //            $scope.labels = ["Proteínas", "Carboidratos", "Gorduras"];
 //            $scope.data = [300, 500, 100];
 //            $scope.colours = ["#FF9900", "#FFE500", "#CCCCCC"];
 //            $scope.options = {
 //            animationEasing: "easeOutQuart",
 //            showTooltips: false,
 //            percentageInnerCutout: 93,
 //            segmentShowStroke : false,
 //            onAnimationProgress: function () {
 //
 //            this.chart.ctx.font = '300 2.5rem Helvetica Neue';
 //            this.chart.ctx.fillStyle = '#666';
 //            this.chart.ctx.textAlign = 'center';
 //            this.chart.ctx.textBaseline = 'middle';
 //
 //            var valueLabel = this.segments[0].value + this.segments[1].value;
 //
 //            var x = this.chart.canvas.clientWidth / 2;
 //            var y = this.chart.canvas.clientHeight / 2;
 //
 //            var x_fix = 0;
 //            var y_fix = 0;
 //
 //            this.chart.ctx.fillText(valueLabel, x + x_fix, y + y_fix - 10);
 //
 //            if($(document).height() < 600){
 //                $('#chart-total-label').css({
 //                 'margin-top': '-5rem',
 //                 'padding-bottom': '5.5rem'
 //                });
 //            }
 //                $('#chart-total-label').css('visibility', 'visible');
 //            }
 //
 //            };


             if($(document).height() < 600)
                 $('.goals-container .chart-container canvas').css('height','200px');

             $scope.renderProgress = function(progress){
             progress = Math.floor(progress);
             if(progress<25){
             var angle = -90 + (progress/100)*360;
             $(".animate-0-25-b").css({"transform":"rotate("+angle+"deg)", "transition":"transform 0.7s ease"});
             }
             else if(progress>=25 && progress<50){
             var angle = -90 + ((progress-25)/100)*360;
             $(".animate-0-25-b").css({"transform":"rotate(0deg)", "transition":"transform 0.7s ease"});
             $(".animate-25-50-b").css({"transform":"rotate("+angle+"deg)", "transition":"transform 0.7s ease"});
             }
             else if(progress>=50 && progress<75){
             var angle = -90 + ((progress-50)/100)*360;
             $(".animate-25-50-b, .animate-0-25-b").css({"transform":"rotate(0deg)", "transition":"transform 0.7s ease"});
             $(".animate-50-75-b").css({"transform":"rotate("+angle+"deg)", "transition":"transform 0.7s ease"});
             }
             else if(progress>=75 && progress<=100){
             var angle = -90 + ((progress-75)/100)*360;
             $(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b")
             .css({"transform":"rotate(0deg)", "transition":"transform 0.7s ease"});
             $(".animate-75-100-b").css({"transform":"rotate("+angle+"deg)", "transition":"transform 0.7s ease"});
             }
             }

             $scope.slideHasChanged = function(index){
                 if(index == 2){
                     $timeout(function(){
                      $scope.renderProgress(87);
                      }, 100);
                 }else if(index == 1){
                     $('.circle-container').css('margin-left', '20%');
                     $('.weight-lost').css('width', '30%');
                 }
             }
             //Radar chart

 //            $scope.labels =["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"];
 //
 //            $scope.data = [
 //                           [65, 59, 90, 81, 56, 55, 40],
 //                           [28, 48, 40, 19, 96, 27, 100]
 //                           ];

             $scope.previousSlide = function(){
                 $ionicSlideBoxDelegate.previous();
             }

             $scope.nextSlide = function(){
                 $ionicSlideBoxDelegate.next();
             }

             $scope.changePicture = function(){
                 $ionicActionSheet.show({
                                    buttons: [
                                              { text: 'Carregar Foto do Dispositivo' },
                                              { text: 'Tirar Foto' },
                                              { text: 'Ver Foto' }
                                              ],
                                    destructiveText: 'Remover Foto',
                                    titleText: 'Foto de Perfil',
                                    cancelText: 'Cancelar',
                                    cancel: function() {

                                    },
                                    buttonClicked: function(index) {
                                        switch(index){
                                         case 0:
                                             $scope.pickImage();
                                             break;
                                         case 1:
                                             $scope.takePicture();
                                             break;
                                        }
                                        return true;
                                    }
                                    });
             }

             $scope.pickImage = function(){
                 var options = {
                     maximumImagesCount: 10,
                     width: 800,
                     height: 800,
                     quality: 80
                 };

                 $cordovaImagePicker.getPictures(options)
                 .then(function (results) {
                   for (var i = 0; i < results.length; i++) {
                     document.getElementById('profile-img').style.backgroundImage = 'url('+results[i]+')';
                   }
                   }, function(error) {
                       alert('Não foi possível pegar uma imagem');
                   });
             }

             $scope.takePicture = function(){
                 var options = {
                     destinationType: Camera.DestinationType.FILE_URI,
                     sourceType: Camera.PictureSourceType.CAMERA,
                 };

                 $cordovaCamera.getPicture(options).then(function(imageURI) {
                     document.getElementById('profile-img').style.backgroundImage = 'url('+imageURI+')';
                 }, function(err) {

                 });
             }

             $scope.recipes = function(){
                 $state.go('app.recipes');
             }

             $ionicModal.fromTemplateUrl('templates/weight-modal.html', {
                     scope: $scope
             }).then(function(modal) {
                     $scope.modal = modal;
             });

             $scope.weightModal = function(){

                 $scope.weights = [
                                   { icon: 'ion-arrow-down-a green', number: '77.9kg', date: '30/10', id: 5 },
                                   { icon: 'ion-minus-round light-orange', number: '79.5kg', date: '21/10', id: 4 },
                                   { icon: 'ion-arrow-up-a red', number: '79.5kg', date: '14/10', id: 3 },
                                   { icon: 'ion-arrow-down-a green', number: '79kg', date: '07/10', id: 2 },
                                   { icon: 'ion-minus-round gray', number: '80kg', date: '01/10 - início', id: 1 }
                 ];

                 $scope.modal.show();

             $scope.lineLabels = ["01/10", "07/10", "14/10", "21/10", "30/10"];
             $scope.lineData = [[80, 79, 79.5, 79.5, 77.9]];
             $scope.lineColours = ["#ffbf05", "#FFE500", "#CCCCCC"];
             $scope.lineSeries = ['Peso'];
             $scope.lineOptions = {
             bezierCurve : false,
             scaleShowGridLines : false,
             scaleLineColor : "rgba(225, 225, 225, 0.29)",
             scaleShowHorizontalLines: false,
             responsive: true,
             scaleShowVerticalLines: false,
             scaleStartValue: 0
             };
             };

             $scope.updateWeight = function(){
             var myPopup = $ionicPopup.show({
                                            template: '<input type="text">',
                                            title: 'Atualize seu peso',
                                            subTitle: 'Insira abaixo o seu peso atual',
                                            scope: $scope,
                                            buttons: [
                                                      {
                                                      text: 'Cancelar',
                                                      type: 'button-clear'
                                                      },
                                                      {
                                                      text: '<b>Registrar</b>',
                                                      type: 'button-clear',
                                                      onTap: function(e) {
                                                      }
                                                      }
                                                      ]
                                            });
             myPopup.then(function(res) {
                          console.log(res);
                          });
             };

             $scope.closeModal = function() {
                 $scope.modal.hide();
             };
 })

 .controller('TimelineCtrl', function($scope, $http, $ionicActionSheet, $cordovaImagePicker, $cordovaCamera, $timeout, $compile) {
             var user_info = localStorage.getItem('user_info');

             var username = JSON.parse(user_info).user_info.username;
             var user = JSON.parse(user_info).user_info.user;
             var picture = JSON.parse(user_info).user_info.picture;

             if(user_info){
                 $('.profile-block #timeline-profile-img').css('background-image', 'url('+picture+')');
                 $('.profile-block #timeline-profile-name').html(user);
             }else{
                 $state.go('login');
             }


             $scope.doRefresh = function() {
 //            $http.get('/new-items')
 //            .success(function(newItems) {
 //                     $scope.items = newItems;
 //                     })
 //            .finally(function() {
 //                     $scope.$broadcast('scroll.refreshComplete');
 //                     });
             $timeout(function() {
                      $scope.$broadcast('scroll.refreshComplete');
             }, 3000);
             }

             $scope.photoToShare = function(){
                 $ionicActionSheet.show({
                                    buttons: [
                                              { text: 'Carregar Foto do Dispositivo' },
                                              { text: 'Tirar Foto' },
                                              { text: 'Antes e Depois' }
                                              ],
                                    titleText: 'Compartilhar Foto',
                                    cancelText: 'Cancelar',
                                    cancel: function() {

                                    },
                                    buttonClicked: function(index) {
                                    switch(index){
                                    case 0:
                                    $scope.pickImage();
                                    break;
                                    case 1:
                                    $scope.takePicture();
                                    break;
                                    case 2:
                                    $scope.beforeAfter(0);
                                    }
                                    return true;
                                    }
                 });
             }

             $scope.pickImage = function(){
             var options = {
             maximumImagesCount: 10,
             width: 800,
             height: 800,
             quality: 80
             };

             $cordovaImagePicker.getPictures(options)
             .then(function (results) {
                   for (var i = 0; i < results.length; i++) {
                   document.getElementById('photo-to-share').style.backgroundImage = 'url('+results[i]+')';
                   document.getElementById('photo-to-share').style.display = 'block';
                   }
                   }, function(error) {
                   alert('Não foi possível pegar uma imagem');
                   });
             }

             $scope.takePicture = function(){
             var options = {
             destinationType: Camera.DestinationType.FILE_URI,
             sourceType: Camera.PictureSourceType.CAMERA,
             correctOrientation: true,
             encodingType: Camera.EncodingType.JPEG,
             targetWidth: 720
             };

             $cordovaCamera.getPicture(options).then(function(imageURI) {
                                                     document.getElementById('photo-to-share').style.backgroundImage = 'url('+imageURI+')';
                                                     document.getElementById('photo-to-share').style.display = 'block';
                                                     }, function(err) {

                                                     });
             }

             $scope.beforeAfter = function(index){
             var options = {
             destinationType: Camera.DestinationType.FILE_URI,
             sourceType: Camera.PictureSourceType.CAMERA,
             correctOrientation: true,
             encodingType: Camera.EncodingType.JPEG,
             targetWidth: 720
             };

             $cordovaCamera.getPicture(options).then(function(imageURI) {
                                                     document.getElementById('photo-to-share-'+index).style.backgroundImage = 'url('+imageURI+')';
                                                     document.getElementById('photo-to-share-'+index).style.display = 'block';

                                                     if(index == 0)
                                                     $scope.beforeAfter(1);

                                                     }, function(err) {

                                                     });
             }

             $scope.post = function(){
             var postStatus = $('.profile-block').clone();

             var compiledElement = $compile(postStatus)($scope);

             postStatus.find('.photo-to-share').css({
                                                    'background-image': 'none',
                                                    'display': 'none'
                                                    });

             var postContent = $('.cd-timeline-block.profile-block textarea').val();
             $('.cd-timeline-block.profile-block textarea').parent().parent().parent().append('<p>'+postContent+'</p>');

             $('.cd-timeline-block.profile-block textarea').parent().parent().remove();

             $('.cd-timeline-block.profile-block .cd-date').fadeIn();

             $('.cd-timeline-block.profile-block .ion-ios-heart-outline').fadeIn();

             $('.cd-timeline-block.profile-block').removeClass('profile-block recent-post');

             $('.profile-img').removeClass('profile-img-share');

             $('.share-options').css({
                                     'transform': 'scaleY(0)',
                                     'transform-origin': 'top',
                                     'transition': 'transform 0.5s ease'
                                     }).remove();

             setTimeout(function(){
                        postStatus.addClass('recent-post').css({
                                                               'max-height': '0',
                                                               'transform': 'scaleY(0)',
                                                               'transform-origin': 'top',
                                                               'transition': 'all 0.1s ease'
                                                               });

                        $('.cd-container').prepend(postStatus);

                        setTimeout(function(){


                                   $('.recent-post').css({
                                                         'max-height': '500px',
                                                         'transform': 'scaleY(1)',
                                                         'transform-origin': 'top',
                                                         'transition': 'all 0.3s cubic-bezier(.44,.44,.74,.73)'
                                                         });
                                   }, 1);
                        }, 100);
             }

 })

 .controller('RecipesCtrl', function($scope, $state, Authorization){
     $scope.recipes = [
         { image: 'img/bolo_cenoura.png', title: 'Bolo de Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 1 },
         { image: 'img/bife_cenoura.png', title: 'Bife Enrolado com Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 2 },
         { image: 'img/hamburguer_cenoura.png', title: 'Hamburguer de Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 3 },
         { image: 'img/salada_cenoura.png', title: 'Salada de grão-de-bico e cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 4 },
         { image: 'img/salada_frango_cenoura.png', title: 'Salada de Frango com Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 5 },
         { image: 'img/suco_cenoura.png', title: 'Suco de Cenoura com Beterraba', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 6 },
         { image: 'img/suco_laranja_cenoura.png', title: 'Suco de Laranja com Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 7 },
         { image: 'img/sufle_cenoura.png', title: 'Suflê de Cenoura', description: 'Deliciosa e simples de preparar', id: 8 },
         { image: 'img/bolo_cenoura.png', title: 'Bolo de Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 9 },
         { image: 'img/bolo_cenoura.png', title: 'Bolo de Cenoura', description: 'Excelente opção para um chá da tarde ou café da manhã', id: 10 }
     ];

             $scope.teste = Authorization;
             console.log($scope.teste);
     $scope.doRefresh = function() {
         $timeout(function() {
             $scope.$broadcast('scroll.refreshComplete');
         }, 3000);
     }
 })

 .controller('RecipeCtrl', function($scope, $stateParams, $ionicHistory, Authorization){
             $scope.teste = Authorization;

             console.log($scope.teste);
             console.log($stateParams);
             console.log($stateParams.recipeId);

             $scope.renderProgress = function(progress){
             progress = Math.floor(progress);
             if(progress<25){
             var angle = -90 + (progress/100)*360;
             $(".animate-0-25-b").css("transform","rotate("+angle+"deg)");
             }
             else if(progress>=25 && progress<50){
             var angle = -90 + ((progress-25)/100)*360;
             $(".animate-0-25-b").css("transform","rotate(0deg)");
             $(".animate-25-50-b").css("transform","rotate("+angle+"deg)");
             }
             else if(progress>=50 && progress<75){
             var angle = -90 + ((progress-50)/100)*360;
             $(".animate-25-50-b, .animate-0-25-b").css("transform","rotate(0deg)");
             $(".animate-50-75-b").css("transform","rotate("+angle+"deg)");
             }
             else if(progress>=75 && progress<=100){
             var angle = -90 + ((progress-75)/100)*360;
             $(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b")
             .css("transform","rotate(0deg)");
             $(".animate-75-100-b").css("transform","rotate("+angle+"deg)");
             }
             $(".text").html("1500/2000<br>calorias");
             }

             $scope.renderProgress(75);
 });


